dict-toolbox>=2.1.2
lazy_object_proxy
pop-config>=8.0.2
pop-loop>=1.0.5,<2.0.0
PyYAML>=6.0.1
