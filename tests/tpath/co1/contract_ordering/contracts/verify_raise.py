"""
A contract that raises an error in its __verify_order__ check
"""


def __verify_order__(hub, contracts: list) -> tuple:
    raise ValueError("Failed order verification")


def pre_acc(hub, ctx):
    hub.co.PRE_ORDER.append(__name__)


def post_acc(hub, ctx):
    ctx.ret.append(__name__)
    return ctx.ret
