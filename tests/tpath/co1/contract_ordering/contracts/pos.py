"""
A contract with a positive order value that should come second
"""
__order__ = 2


def pre_acc(hub, ctx):
    hub.co.PRE_ORDER.append(__name__)


def post_acc(hub, ctx):
    ctx.ret.append(__name__)
    return ctx.ret
