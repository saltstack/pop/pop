# pylint: disable=expression-not-assigned
import pytest

import pop.exc
import pop.hub

# Import pop


@pytest.fixture(scope="function")
def hub():
    yield pop.hub.Hub()


def test_sub_alias(hub):
    hub.pop.sub.add("tests.alias")
    assert hub.alias.init.ping() is True
    assert hub.red.init.ping() is True
    assert hub.blue.init.ping() is True
    assert hub.green.init.ping() is True


def test_basic(hub):
    hub.pop.sub.add("tests.mods")
    hub.mods.test.ping()
    assert hub.mods.test.ping() == {}
    assert hub.mods.test.demo() is False
    assert hub.mods.test.ping() == hub.mods.foo.bar()


def test_subdirs(hub):
    hub.pop.sub.add("tests.sdirs")
    hub.pop.sub.load_subdirs(hub.sdirs)
    assert hub.sdirs.test.ping()
    assert hub.sdirs.l11.test.ping()
    assert hub.sdirs.l12.test.ping()
    assert hub.sdirs.l13.test.ping()


def test_subdirs_recurse(hub):
    hub.pop.sub.add("tests.sdirs")
    hub.pop.sub.load_subdirs(hub.sdirs, recurse=True)
    assert hub.sdirs.test.ping()
    assert hub.sdirs.l11.test.ping()
    assert hub.sdirs.l11.l2.test.ping()
    assert hub.sdirs.l12.l2.test.ping()
    assert hub.sdirs.l13.l2.test.ping()


def test_getattr(hub):
    hub.pop.sub.add("tests.mods")
    hub.mods.test.ping()
    assert getattr(hub, "mods.test.ping")() == {}
    assert getattr(hub.mods.test, "demo")() is False
    assert hub.mods.test.ping() == getattr(hub, "mods.foo.bar")()


def test_iter_sub(hub):
    hub.pop.sub.add("tests.mods")
    mods = []
    for mod in hub.mods:
        mods.append(mod.__name__)
    assert mods == sorted(hub.mods._loaded.keys())


def test_iter_subs_rec(hub):
    hub.pop.sub.add("tests.sdirs")
    hub.pop.sub.load_subdirs(hub.sdirs, recurse=True)
    subs = []
    for sub in hub.pop.sub.iter_subs(hub.sdirs, recurse=True):
        subs.append(sub._subname)
    assert subs == ["l11", "l2", "l12", "l2", "l13", "l2"]


def test_iter_loads(hub):
    hub.pop.sub.add("tests.mods.iter")
    for mod in hub.iter:
        if mod.__name__ == "init":
            continue
        mod.run()
    assert hub.iter.DATA == {"bar": True, "foo": True}


def test_iter_sub_nested(hub):
    hub.pop.sub.add("tests.mods")
    mods = []
    for _ in hub.mods:
        for mod in hub.mods:
            mods.append(mod.__name__)
        break
    assert mods == sorted(hub.mods._loaded.keys())


def test_iter_hub(hub):
    hub.pop.sub.add("tests.mods")
    subs = []
    for sub in hub:
        subs.append(sub._subname)
    assert subs == sorted(hub._subs.keys())


def test_iter_hub_nested(hub):
    hub.pop.sub.add("tests.mods")
    subs = []
    for _ in hub:
        for sub in hub:
            subs.append(sub._subname)
        break
    assert subs == sorted(hub._subs.keys())


def test_iter_vars(hub):
    hub.pop.sub.add("tests.mods")
    funcs = []
    for var in hub.pop.sub:
        funcs.append(var.__name__)
    assert funcs == sorted(hub.pop.sub._funcs.keys())


def test_iter_vars_nested(hub):
    hub.pop.sub.add("tests.mods")
    funcs = []
    for _ in hub.pop.sub:
        for var in hub.pop.sub:
            funcs.append(var.__name__)
        break
    assert funcs == sorted(hub.pop.sub._funcs.keys())


def test_nest(hub):
    """
    Test the ability to nest the subs in a deeper namespace
    """
    hub.pop.sub.add("tests.mods")
    hub.pop.sub.add("tests.mods.nest", sub=hub.mods)
    assert hub.mods.nest.basic.ret_true()


def test_this(hub):
    hub.pop.sub.add("tests.mods")
    hub.mods.test.ping()
    assert hub.mods.test.this() == {}


def test_func_attrs(hub):
    hub.pop.sub.add("tests.mods")
    assert "bar" in hub.mods.test.attr.__dict__
    assert hub.mods.test.attr.func.bar is True
    assert hub.mods.test.attr.func.func is not hub.mods.test.attr.func


def test_ref_sys(hub):
    hub.pop.sub.add("tests.mods")
    hub.mods.test.ping()
    assert hub.pop.ref.last("mods.test.ping")() == {}
    path = hub.pop.ref.path("mods.test.ping")
    assert len(path) == 4
    assert hasattr(path[0], "mods")
    assert hasattr(path[1], "test")
    assert hasattr(path[2], "ping")
    rname = "Made It!"
    hub.pop.ref.create("mods.test.Foo", rname)
    assert hub.mods.test.Foo == rname


def test_module_level_direct_call(hub):
    hub.pop.sub.add("tests.mods")
    with pytest.raises(Exception):
        hub.mods.test.module_level_non_aliased_ping_call()
    assert hub.mods.test.module_level_non_aliased_ping_call_fw_hub() == {}


def test_contract(hub):
    hub.pop.sub.add("tests.mods", contracts_pypath="tests.contracts")
    with pytest.raises(Exception) as context:
        hub.mods.test.ping(4)


def test_inline_contract(hub):
    hub.pop.sub.add("tests.cmods")
    assert hub.cmods.ctest.cping()
    assert hub.CPING


def test_no_contract(hub):
    hub.pop.sub.add("tests.mods")
    with pytest.raises(TypeError) as context:
        hub.mods.test.ping(4)


def test_contract_manipulate(hub):
    hub.pop.sub.add("tests.mods", contracts_pypath="tests.contracts")
    assert "override" in hub.mods.all.list()
    assert "post called" in hub.mods.all.list()
    assert "post" in hub.mods.all.dict()


def test_contract_sigs(hub):
    with pytest.raises(pop.exc.ContractSigException) as exc:
        hub.pop.sub.add("tests.csigs")
        hub.csigs.sigs.first(4, 6, 8)
    errs = exc.value.args[0].splitlines()

    assert "Signature Errors" in errs[0]
    filtered_errs = set({e for e in errs[1:-2] if "Enforcing signature" not in e})
    assert filtered_errs == {
        "first: **kwargs are not permitted as a parameter",
        'first: Parameter "z" does not have the correct name: b',
        'second: Parameter "a" is past available positional params',
        'six: Parameter "args" is not in the correct position for *args',
        'first: Parameter "a" is type "<class \'inspect._empty\'>" not "<class \'str\'>"',
        'first: Parameter "c" is type "<class \'str\'>" not "typing.List"',
        'seven: Parameter "bar" is past available positional params',
    }

    assert "Enforcing signature" in errs[-3]
    assert "Signature Functions Missing" in errs[-2]
    assert "Function 'missing' is missing from" in errs[-1]


def test_private_function_cross_access(hub):
    hub.opts = "OPTS!"
    hub.pop.sub.add("tests.mods")
    # Let's make sure that the private function is not accessible through the sub
    with pytest.raises(AttributeError) as exc:
        hub.mods.priv._private() == "OPTS!"

    # Let's confirm that the private function has access to the cross
    # objects
    assert hub.mods.priv.public() == "OPTS!"


def test_private_function_cross_access_with_contracts(hub):
    hub.opts = "OPTS!"
    hub.pop.sub.add("tests.mods", contracts_pypath="tests.contracts")
    # Let's make sure that the private function is not accessible through the sub
    with pytest.raises(AttributeError) as exc:
        hub.mods.priv._private() == "OPTS!"

    # Let's confirm that the private function has access to the cross objects
    assert hub.mods.priv.public() == "OPTS!"


def test_cross_in_virtual(hub):
    hub.opts = "OPTS!"
    hub.pop.sub.add("tests.mods", contracts_pypath="tests.contracts")
    assert hub.mods.virt.present() is True


def test_virtual_ret_true(hub):
    hub.opts = "OPTS!"
    hub.pop.sub.add("tests.mods", contracts_pypath="tests.contracts")
    assert hub.mods.truev.present() is True


def test_mod_init():
    hub = pop.hub.Hub()
    hub.context = {}
    hub.pop.sub.add(
        pypath="tests.mods.subinit",
        subname="mods",
        contracts_pypath="tests.contracts",
        load_all=True,
    )
    assert hub.context == {"init": True, "subinit": True}
    assert hub.mods.init.inited() is True
    assert hub.mods.subinit.inited() is True

    # Now without force loading, at least a function needs to be called
    hub = pop.hub.Hub()
    hub.context = {}
    hub.pop.sub.add(
        pypath="tests.mods.subinit",
        subname="mods",
        contracts_pypath="tests.contracts",
        load_all=False,
    )
    assert hub.context == {"init": True}
    assert hub.mods.subinit.inited() is True
    assert hub.context == {"init": True, "subinit": True}


def test_sub_init(hub):
    hub.context = {}
    hub.pop.sub.add(
        pypath="tests.mods.subinit", subname="mods", contracts_pypath="tests.contracts"
    )
    assert hub.mods.init.inited() is True


def test_non_module_functions_are_not_loaded(hub):
    hub.pop.sub.add("tests.mods")
    hub.mods._load_all()
    assert "scan" not in dir(hub.mods.test)
    hub.mods.test.call_scan() is True


def test_double_underscore(hub):
    hub.pop.sub.add("tests.mods")
    hub.mods.test.double_underscore()


def test_unique_name(hub):
    """
    Verify that the assigned module name inside of the python sys.modules
    is unique
    """
    hub.pop.sub.add(dyne_name="dyne1")
    mname = hub.dyne3.init.mod_name()
    assert not mname.startswith(".")


def test_dyne(hub):
    hub.pop.sub.add(dyne_name="dyne1")
    assert hub.dyne1.INIT
    assert hub.dyne2.INIT
    assert hub.dyne3.INIT
    assert hub.dyne1.test.dyne_ping()
    assert hub.dyne1.nest.nest_dyne_ping()
    assert hub.dyne2.test.dyne_ping()
    assert hub.dyne2.nest.nest_dyne_ping()
    assert hub.dyne3.test.dyne_ping()
    assert hub.dyne3.nest.nest_dyne_ping()


def test_sub_virtual(hub):
    hub.pop.sub.add(dyne_name="dyne4")
    hub.pop.sub.load_subdirs(hub.dyne4)
    assert "nest" in hub.dyne4._subs
    assert "nest" not in hub.dyne4._loaded


def test_dyne_nest(hub):
    hub.pop.sub.add(dyne_name="dn1")
    hub.pop.sub.load_subdirs(hub.dn1, recurse=True)
    assert hub.dn1.nest.dn1.ping()
    assert hub.dn1.nest.dn2.ping()
    assert hub.dn1.nest.dn3.ping()
    assert hub.dn1.nest.next.test.ping()
    assert hub.dn1.nest.next.last.test.ping()


def test_existing_dyne(hub):
    hub.pop.sub.add(dyne_name="dn1")
    hub.pop.sub.load_subdirs(hub.dn1, recurse=True)
    assert "nest" in hub.dn1._subs
    assert "nest" not in hub.dn1._loaded
    # Add the same dyne again without recursing it and verify that the recursive elements are still there
    hub.pop.sub.add(dyne_name="dn1")
    assert "nest" in hub.dn1._subs
    assert "nest" not in hub.dn1._loaded


def test_dyne_extend(hub):
    hub.pop.sub.add(dyne_name="dn1")
    hub.pop.sub.load_subdirs(hub.dn1, recurse=True)
    assert hub.dn1.nest.over.in_dn1()
    assert hub.dn1.nest.over.in_dn2()
    assert hub.dn1.nest.over.in_dn3()


def test_dyne_overwrite(hub):
    hub.pop.sub.add(dyne_name="dn1")
    hub.pop.sub.load_subdirs(hub.dn1, recurse=True)
    # Assure that the first instance of a function gets overwritten
    assert hub.dn1.nest.over.source() == "dn2"


def test_dyne_plugins(hub):
    hub.pop.sub.add(dyne_name="dn1")
    hub.pop.dyne.get()
    hub.pop.dyne.refresh()
    dyne = hub.pop.dyne.get()
    assert "dyne1" in dyne
    assert "dyne2" in dyne
    assert "dyne3" in dyne


def test_dict_update(hub):
    ret = hub.pop.dicts.update({}, {})
    assert ret == {}


def test_contract_signatures(hub):
    hub.LOAD_PASS = True
    hub.LOAD_FAIL = False
    # These functions should load no problem
    hub.pop.sub.add("tests.mods.contract_sig")


def test_contract_signature_fail(hub):
    hub.LOAD_PASS = False
    hub.LOAD_FAIL = True
    # These functions should load with sig failures
    with pytest.raises(pop.exc.ContractSigException) as e:
        hub.pop.sub.add("tests.mods.contract_sig")
    errs = e.value.args[0].splitlines()

    assert "Signature Errors" in errs[0]

    filtered_errs = set({e for e in errs[1:-1] if "Enforcing signature" not in e})
    assert filtered_errs == {
        "args: **kwargs are not permitted as a parameter",
        "async_func: Function must be asynchronous",
        "kwargs: *args are not permitted as a parameter",
    }
