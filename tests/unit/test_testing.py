import pytest
from pytest_pop.mods import testing


class TestStripHub:
    def test_basic(self):
        def f(hub):
            pass

        g = testing.strip_hub(f)

        g()
        with pytest.raises(TypeError, match=r"f\(\) takes 0 positional arguments"):
            g("bar")

    def test_param(self):
        def f(hub, foo):
            pass

        g = testing.strip_hub(f)

        g("foo")
        g(foo="foo")
        with pytest.raises(TypeError, match=r"f\(\) missing 1 required positional"):
            g()

    def test_defaults(self):
        def f(hub, foo=None):
            pass

        g = testing.strip_hub(f)

        g()
        g("foo")
        g(foo="foo")

    def test_kwargs(self):
        def f(hub, arg, **kwargs):
            pass

        g = testing.strip_hub(f)

        g("arg")
        g("arg", kwarg1="arg1")
        g(arg="arg", kwarg1="arg1")

    def test_args(self):
        def f(hub, *args):
            pass

        g = testing.strip_hub(f)

        g()
        g("an arg")
        g("another arg")

    def test_keyword_only(self):
        def f(hub, *args, foo="foo"):
            pass

        g = testing.strip_hub(f)

        g("arg", foo="other foo")
        g(foo="other foo")
        with pytest.raises(
            TypeError, match=r"f\(\) got an unexpected keyword argument"
        ):
            g(baz="baz")
