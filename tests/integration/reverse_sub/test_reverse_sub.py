import os
import pathlib
import tempfile
from unittest import mock

import pytest

import pop.hub
import pop.loader

CONTRACTS = """
def sig(hub, *args, **kwargs):
    ...

def pre(hub, ctx):
    print("contract-pre")

def post(hub, ctx):
    print("contract-post")
    if not hasattr(ctx, "original_ret"):
        ctx.original_ret = ctx.ret
    return ctx

def call(hub, ctx):
    print('contract-pre-call')
    result = ctx.func(*ctx.args, **ctx.kwargs)
    print('contract-post-call')
    return result
"""

RECURSIVE_CONTRACTS = """
def sig(hub, *args, **kwargs):
    ...

def pre(hub, ctx):
    print("recursive-pre")

def post(hub, ctx):
    print("recursive-post")
    if not hasattr(ctx, "original_ret"):
        ctx.original_ret = ctx.ret
    return ctx

def call(hub, ctx):
    print('recursive-pre-call')
    result = ctx.func(*ctx.args, **ctx.kwargs)
    print('recursive-post-call')
    return result
"""


@pytest.fixture
def temp_sub_hub():
    pwd = os.getcwd()
    try:
        with tempfile.TemporaryDirectory() as tempdir:
            os.chdir(tempdir)
            sub = pathlib.Path(tempdir) / "test_reverse_sub" / "test_sub"
            sub.mkdir(parents=True, exist_ok=True)
            conf = sub.parent / "conf.py"
            conf.write_text('DYNE={"test_sub": ["test_sub"]}')
            init = sub / "init.py"
            init.write_text("def func(hub, *args, **kwargs):\n    return args, kwargs")
            contracts = sub / "contracts"
            contracts.mkdir()
            (contracts / "init.py").write_text(CONTRACTS)
            recursive_contracts = sub / "recursive_contracts"
            recursive_contracts.mkdir()
            (recursive_contracts / "init.py").write_text(RECURSIVE_CONTRACTS)

            with mock.patch("sys.path", [str(sub.parent.parent)]):
                hub = pop.hub.Hub()
                hub.pop.sub.add(dyne_name="test_sub")
                yield hub
    finally:
        os.chdir(pwd)


def test_example():
    """
    Test the example used in the docs
    """
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="my_dyne")

    context = "An object that will be universally useful for resolving references under the ReverseSub"

    def _resolver(ref: str, context):
        # return a callable based on the reference and context
        return lambda *a, **kw: (a, kw, ref, 1)

    hub.pop.sub.dynamic(
        sub=hub.my_dyne, subname="my_sub", resolver=_resolver, context=context
    )

    ret = hub.my_dyne.my_sub.foo.bar.baz("arg1", kwarg="value1")
    assert ret == (("arg1",), {"kwarg": "value1"}, "my_sub.foo.bar.baz", 1)


def resolver(ref, context):
    def resolved_func(*args, **kwargs):
        return ref, context, args, kwargs

    return resolved_func


def test_resolver():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="dynamic")
    context = object()

    _resolver = mock.MagicMock()
    _func = mock.MagicMock(__name__="func")
    _resolver.return_value = _func

    hub.pop.sub.dynamic(
        subname="foo", sub=hub.dynamic, resolver=_resolver, context=context
    )

    hub.dynamic.foo.bar.baz(1, 2, 3, a=1, b=2, c=3)

    _resolver.assert_called_once_with("foo.bar.baz", context)
    _func.assert_called_once_with(1, 2, 3, a=1, b=2, c=3)


def test_full():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="dynamic")
    context = object()

    hub.pop.sub.dynamic(
        subname="foo", sub=hub.dynamic, context=context, resolver=resolver
    )

    ret = hub.dynamic.foo.bar.baz.bop(0, 1, 2, a=1, b=2, c=3)
    assert ret == (
        "foo.bar.baz.bop",
        context,
        (0, 1, 2),
        {"a": 1, "b": 2, "c": 3},
    )


def test_dot():
    hub = pop.hub.Hub()
    hub.pop.sub.add(subname="dynamic")
    context = object()

    hub.pop.sub.dynamic(
        subname="foo", sub=hub.dynamic, context=context, resolver=resolver
    )

    # Use a getitem that has a dot in the reference on a ReverseSub
    ret = hub.dynamic.foo["bar.baz"].bop(0, 1, 2, a=1, b=2, c=3)
    assert ret == (
        "foo.bar.baz.bop",
        context,
        (0, 1, 2),
        {"a": 1, "b": 2, "c": 3},
    )


def test_sub_dot():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="dynamic")
    context = object()

    hub.pop.sub.dynamic(
        subname="foo", sub=hub.dynamic, context=context, resolver=resolver
    )

    # Use a getitem that has a dot in the reference on a Sub containing ReverseSubs
    ret = hub.dynamic["foo.bar.baz"].bop(0, 1, 2, a=1, b=2, c=3)
    assert ret == (
        "foo.bar.baz.bop",
        context,
        (0, 1, 2),
        {"a": 1, "b": 2, "c": 3},
    )


def test_directly_on_hub():
    hub = pop.hub.Hub()

    hub.pop.sub.dynamic(subname="dynamic", resolver=resolver)

    ret = hub.dynamic.foo.bar(1, 2, 3, a=1, b=2, c=3)
    assert ret == ("dynamic.foo.bar", None, (1, 2, 3), {"a": 1, "b": 2, "c": 3})


def test_contains():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="dynamic")

    def resolves(ref, context):
        return lambda: 0

    hub.pop.sub.dynamic(subname="foo", sub=hub.dynamic, resolver=resolves)

    assert "baz" in hub.dynamic.foo


def test_does_not_contain():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="dynamic")

    def does_not_resolve(ref, context):
        # This Resolver doesn't find anything, no return
        ...

    hub.pop.sub.dynamic(subname="foo", sub=hub.dynamic, resolver=does_not_resolve)

    assert "baz" not in hub.dynamic.foo


def test_iter():
    hub = pop.hub.Hub()
    hub.pop.sub.dynamic(subname="dynamic", sub=hub, resolver=lambda: None)
    for sub in hub.dynamic:
        raise IndexError(f"Dynamic subs should not be iterable, found: '{sub}'")


def test_next():
    hub = pop.hub.Hub()
    hub.pop.sub.dynamic(subname="dynamic", sub=hub, resolver=lambda: None)
    with pytest.raises(StopIteration):
        next(iter(hub.dynamic))


def test_self_referencing():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="gcp")
    hub.pop.sub.add(sub=hub.gcp, subname="init")

    # One real function exists native to the ReverseSub
    func = mock.MagicMock(__name__="func")
    hub.gcp.init.func = func

    def _reflexive_resolver(ref, context):
        # The resolver returns a real function from a sibling sub
        return hub.gcp.init.func

    hub.pop.sub.dynamic(subname="foo", sub=hub.gcp, resolver=_reflexive_resolver)

    hub.gcp.foo.bar.baz(0, a=1)

    func.assert_called_once_with(0, a=1)


def test_recursion_error():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="dynamic")

    class context:
        recursed = False

    def _recursive_resolver(ref, context):
        if not context.recursed:
            # If we get to this point again then recursion occurred
            context.recursed = True
            return hub.dynamic.foo.bar()

    hub.pop.sub.dynamic(
        subname="foo", sub=hub.dynamic, resolver=_recursive_resolver, context=context
    )

    with pytest.raises(AttributeError):
        hub.dynamic.foo.bar.baz()


def test_raw():
    resolve = mock.MagicMock()
    func = mock.MagicMock(__name__="func")
    resolve.return_value = func
    context = object()

    sub = pop.hub.ReverseSub(
        pop.hub.Hub(), "reverse", resolver=resolve, context=context
    )

    sub(0, a=1)
    resolve.assert_called_with("reverse", context)
    func.assert_called_once_with(0, a=1)
    func.reset_mock()

    sub.fub(1, b=2)
    resolve.assert_called_with("reverse.fub", context)
    func.assert_called_with(1, b=2)


def test_reflection():
    hub = pop.hub.Hub()

    hub.pop.sub.dynamic(subname="dynamic", resolver=resolver)

    def _reflexive_resolver(ref, context):
        # Return another reverse sub instead of a function
        return hub.dynamic.reverse_sub

    hub.pop.sub.dynamic(subname="reflect", resolver=_reflexive_resolver)

    ret = hub.reflect.asdf(0, a=1)
    assert ret == ("dynamic.reverse_sub", None, (0,), {"a": 1})


def test_overwrite(temp_sub_hub, capsys):
    """
    Verify that real functions on the sub are respected
    """
    hub = temp_sub_hub

    ret = hub.test_sub.init.func(0, a=1)
    assert ret.original_ret == ((0,), {"a": 1})

    # Now that the function is verified to work, add a reverse sub

    def _recursive_resolver(ref, context):
        # The resolver returns a real function from a sibling sub
        return hub.test_sub.init.func

    # Make a dynamic hub where a real sub already exists and has things
    hub.pop.sub.dynamic(subname="test_sub", resolver=_recursive_resolver)

    ret = hub.test_sub.init.func(0, a=1)
    assert ret.original_ret == ((0,), {"a": 1})


def test_recursion(temp_sub_hub, capsys):
    hub = temp_sub_hub

    ret = hub.test_sub.init.func(0, a=1)
    assert ret.original_ret == ((0,), {"a": 1})

    assert capsys.readouterr().out.split() == [
        "contract-pre",
        "recursive-pre",
        "contract-pre-call",
        "contract-post-call",
        "recursive-post",
        "contract-post",
    ]

    # Now that the function is verified to work, add a reverse sub

    def _recursive_resolver(ref, context):
        # The resolver returns a real function from a sibling sub
        return hub.test_sub.init.func

    # Make a dynamic hub where a real sub already exists and has things
    hub.pop.sub.dynamic(subname="test_sub", resolver=_recursive_resolver)

    ret = hub.test_sub.init.func(0, a=1)
    assert ret.original_ret == ((0,), {"a": 1})

    assert capsys.readouterr().out.split() == [
        "contract-pre",
        "recursive-pre",
        "contract-pre-call",
        "contract-post-call",
        "recursive-post",
        "contract-post",
    ]

    ret = hub.test_sub.foo.bar.baz(1, b=2)
    assert ret.original_ret == ((1,), {"b": 2})


def test_no_overwrite(temp_sub_hub, capsys):
    """
    Verify that when a sub with a real presence becomes a ReverseSub that real functions stay
    """
    hub = temp_sub_hub

    ret = hub.test_sub.init.func(0, a=1)
    assert ret.original_ret == ((0,), {"a": 1})

    assert capsys.readouterr().out.split() == [
        "contract-pre",
        "recursive-pre",
        "contract-pre-call",
        "contract-post-call",
        "recursive-post",
        "contract-post",
    ]

    hub.pop.sub.dynamic(subname="test_sub", resolver=resolver)

    ret = hub.test_sub.init.func("untouched result")
    assert ret.original_ret == (("untouched result",), {})

    assert capsys.readouterr().out.split() == [
        "contract-pre",
        "recursive-pre",
        "contract-pre-call",
        "contract-post-call",
        "recursive-post",
        "contract-post",
    ]
    return


def test_contracts(temp_sub_hub, capsys):
    """
    Verify that resolved functions have the recursive contracts of the top ReverseSub
    """
    hub = temp_sub_hub

    ret = hub.test_sub.init.func(0, a=1)
    assert ret.original_ret == ((0,), {"a": 1})

    assert capsys.readouterr().out.split() == [
        "contract-pre",
        "recursive-pre",
        "contract-pre-call",
        "contract-post-call",
        "recursive-post",
        "contract-post",
    ]

    hub.pop.sub.dynamic(subname="asdf", sub=hub.test_sub, resolver=resolver)

    ret = hub.test_sub.asdf.foo.bar(0, a=1)
    assert ret.original_ret == ("asdf.foo.bar", None, (0,), {"a": 1})

    assert capsys.readouterr().out.split() == [
        "recursive-pre",
        "recursive-pre-call",
        "recursive-post-call",
        "recursive-post",
    ]

    ret = hub.test_sub.init.func(1, b=2)
    assert ret.original_ret == ((1,), {"b": 2})


def test_contracts_separated_resolver(capsys):
    # Set up the hub like idem does
    hub = pop.hub.Hub()
    hub.pop.sub.add(pypath="tests.integration.reverse_sub.separated.tool")
    hub.pop.sub.load_subdirs(hub.tool, recurse=True)
    hub.pop.sub.add(pypath="tests.integration.reverse_sub.separated.exec")
    hub.pop.sub.load_subdirs(hub.exec, recurse=True)

    # Run a dynamic function
    ret = hub.exec.mod.foo.bar.func(0, a=1)
    assert ret == ((0,), {"a": 1})

    # Contracts should have had some output
    assert capsys.readouterr().out.split() == [
        "recursive-pre",
        "recursive-pre-call",
        "recursive-post-call",
        "recursive-post",
    ]


def test_contracts_reflexive_resolver(capsys):
    hub = pop.hub.Hub()
    hub.pop.sub.add(pypath="tests.integration.reverse_sub.reflexive.exec")
    hub.pop.sub.load_subdirs(hub.exec, recurse=True)

    ret = hub.exec.mod.foo.bar.func(0, a=1)
    assert ret == ((0,), {"a": 1})

    assert capsys.readouterr().out.split() == [
        "contract-pre",
        "recursive-pre",
        "contract-pre-call",
        "contract-post-call",
        "recursive-post",
        "contract-post",
    ]


def test_sub_ref(temp_sub_hub):
    # hub = pop.hub.Hub()
    hub = temp_sub_hub
    hub.pop.sub.add(dyne_name="dynamic")
    context = object()
    hub.pop.sub.dynamic(
        subname="foo", sub=hub.test_sub, context=context, resolver=resolver
    )

    # ref.__name__ tells the user how to find the object on the hub:
    contracted = hub.pop.sub.add
    assert hub.pop.sub.add == hub[f"{contracted.ref}.{contracted.__name__}"]
    # the above lookup won't work on the ReverseSub, because ref and __name__ would be
    #  resolved as if they were ReverseSubs, but *inside* of ReverseSub.__call__,
    #  the ctx should work the same way:
    ret = hub.test_sub.foo.bar.baz.bop(5)
    assert f"{ret.ref}.{ret.__name__}" == "test_sub.foo.bar.baz.bop"
