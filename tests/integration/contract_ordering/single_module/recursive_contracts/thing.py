def pre(hub, ctx):
    print("recursive-thing-pre")
    ctx.extra = (getattr(ctx, "extra", None) or []) + ["recursive-thing-pre"]


def post(hub, ctx):
    print("recursive-thing-post")
    ctx.ret.append("recursive-thing-post")


def call(hub, ctx):
    print("recursive-thing-pre-call")
    result = ctx.extra + [
        "recursive-thing-pre-call",
        ctx.func(hub=hub),
        "recursive-thing-post-call",
    ]
    print("recursive-thing-post-call")
    return result


def pre_test_fn(hub, ctx):
    print("recursive-thing-pre-test-fn")
    ctx.extra = (getattr(ctx, "extra", None) or []) + ["recursive-thing-pre-test-fn"]


def post_test_fn(hub, ctx):
    print("recursive-thing-post-test-fn")
    ctx.ret.append("recursive-thing-post-test-fn")


def call_test_fn(hub, ctx):
    print("recursive-thing-pre-call-test-fn")
    result = ctx.extra + [
        "recursive-thing-pre-call-test-fn",
        ctx.func(hub=hub),
        "recursive-thing-post-call-test-fn",
    ]
    print("recursive-thing-post-call-test-fn")
    return result
